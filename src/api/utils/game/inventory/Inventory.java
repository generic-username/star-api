package api.utils.game.inventory;

import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;

public class Inventory {

    private org.schema.game.common.data.player.inventory.Inventory internalInventory;
    private InventoryType inventoryType;
    private PlayerState inventoryHolder;

    /**
     * @param internalInventory Internal inventory
     * @param inventoryType Type of inventory
     * @param holder Player who will get updates of modifications (can be null)
     */
    public Inventory(org.schema.game.common.data.player.inventory.Inventory internalInventory, PlayerState holder, InventoryType inventoryType) {
        this.inventoryHolder = holder;
        this.internalInventory = internalInventory;
        this.inventoryType = inventoryType;
    }

    public Inventory(org.schema.game.common.data.player.inventory.Inventory internalInventory, InventoryType inventoryType) {
        this.inventoryHolder = null;
        this.internalInventory = internalInventory;
        this.inventoryType = inventoryType;
    }

    public org.schema.game.common.data.player.inventory.Inventory getInternalInventory() {
        return internalInventory;
    }

    public void setInventoryFilter(InventoryFilter inventoryFilter) {
        org.schema.game.common.data.player.inventory.InventoryFilter internalFilter = internalInventory.getFilter();
        for(ItemStack block : inventoryFilter.getBlocks().keySet()) {
            int amount = inventoryFilter.getBlocks().get(block)[0];
            int upTo = inventoryFilter.getBlocks().get(block)[1];
            //internalFilter.filter.put(?);
            //Todo:Figure out how to put block, amount, and upTo values in the internalFilter
        }
    }

    public void addItem(ItemStack block) {
        int slot = internalInventory.incExistingOrNextFreeSlot(block.getId(), block.getAmount());
        if(inventoryHolder != null) {
            internalInventory.sendInventoryModification(slot);
        }
    }

    public boolean addElementById(short id, int amount) {
        if (ElementKeyMap.isValidType(id)) {
            inventoryHolder.sendInventoryModification(inventoryHolder.getInventory().incExistingOrNextFreeSlot(id, amount), Long.MIN_VALUE);
            return true;
        } else {
            return false;
        }
    }

    public InventoryType getInventoryType() {
        return inventoryType;
    }


}
