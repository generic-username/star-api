package api.utils.callbacks;

import api.entity.StarEntity;

public interface EntitySpawnCallback {
    void onEntitySpawn(StarEntity controller);
}
