package api.element.gui.custom.examples;

import api.element.gui.custom.CustomHudText;
import api.entity.StarEntity;
import org.newdawn.slick.Color;
import org.newdawn.slick.UnicodeFont;
import org.schema.schine.input.InputState;

public class ShipNameElement extends CustomHudText {
    public ShipNameElement(UnicodeFont unicodeFont, InputState inputState) {
        super(100, 20, unicodeFont, Color.gray, inputState);
    }

    public StarEntity entity;
    public void setEntity(StarEntity e){
        entity = e;
    }

    @Override
    public void onInit() {
        super.onInit();
        setTextSimple(new Object(){
            @Override
            public String toString() {
                if(entity == null) return "";
                return entity.getName();
            }
        });
    }
}
