package api.element.gui.custom;

import api.entity.StarEntity;
import api.systems.Reactor;
import org.schema.common.util.StringTools;

public abstract class EntityReactorBar extends CustomHudBar {

    public StarEntity entity;
    public void setEntity(StarEntity e){
        entity = e;
    }

    @Override
    public boolean drawBar() {
        return entity != null;
    }



    @Override
    public float getFilled() {
        if(entity == null){
            return 0;
        }
        Reactor reactor = entity.getCurrentReactor();
        if(reactor == null){
            return 0;
        }
        return (float) reactor.getHpPercent();
    }

    @Override
    public String getText() {
        StarEntity entity = this.entity;
        if(entity != null){
            Reactor reactor = entity.getCurrentReactor();
            if(reactor != null) {
                return "Reactor: [" + StringTools.massFormat(reactor.getHp()) + " / " + StringTools.massFormat(reactor.getMaxHp()) + "]";
            }
        }
        return "Reactor: N/A";
    }
}
